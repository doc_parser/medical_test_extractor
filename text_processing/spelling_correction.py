from unidecode import unidecode
from textdistance import Jaccard
import regex as re

WORD_PATTERN = r'(?i)\b[a-z0-9áàảãạäăắằẳẵặâấầẩẫậéèẻẽẹêếềểễệóòỏõọôốồổỗộơớờởỡợíìỉĩịúùủũụüưứừửữựýỳỷỹỵđ/%]+\b'


def jaccard_correct(input_word, vocabulary):
    if not isinstance(vocabulary, dict):
        vocab = dict((v, 1.0/len(vocabulary)) for v in vocabulary)
    else:
        vocab = vocabulary
    if input_word in vocab:
        return 1.0, vocab.get(input_word), input_word
    else:
        similarities = [
            (1-(Jaccard(qval=2).distance(unidecode(v), unidecode(input_word))), vocab.get(v), v)
            for v in vocab
        ]
        sim, prob, output_word = sorted(similarities)[-1]
        if sim == 0:
            similarities = [(1 - (Jaccard(qval=1).distance(unidecode(v), unidecode(input_word))), vocab.get(v), v)
                            for v in vocab]
            sim, prob, output_word = sorted(similarities)[-1]
        return sim, prob, output_word


def jaccard_correct_str(str_in, vocabulary):
    words = re.findall(WORD_PATTERN, str_in)
    str_out = str_in
    if len(words) > 0:
        for w in words:
            if len(w) < 2 or w.isdigit():
                continue
            sim, prob, cw = jaccard_correct(w, vocabulary)
            str_out = str_out.replace(w, cw)
    # print(f"`{str_in}` >>> `{words}` >>> `{str_out}`")
    return str_out


if __name__ == "__main__":
    vocab = {'tình': 0.2, 'tính': 0.2, 'tín': 0.2, 'tang': 0.2, 'tạng': 0.2}
    print(jaccard_correct('tìn', vocab))