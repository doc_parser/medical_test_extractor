import json
import re
from collections import Counter

import textdistance
from unidecode import unidecode

from data.vocabulary import Vocabulary
from text_processing.edit_distance import levenshtein
from text_processing.spelling_correction import jaccard_correct_str

NUMBER_PATTERN = "[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE^][-+]?\d+)?"
VOCAB = Vocabulary()


def get_all_lab_names(master_data_dict):
    lab_names = dict()
    for key in master_data_dict.keys():
        lab = master_data_dict.get(key)
        syn_names = lab.get("synonyms")
        for name in syn_names:
            lab_names[name] = lab.get("name")
            lab_names[unidecode(name)] = lab.get("name")
    return lab_names


def get_mode(li):
    """

    :param li:
    :return:
    """
    return sorted(Counter(li).items(), key=lambda x: x[1])[-1][0]


def is_header(text_list):
    """

    :param text_list:
    :return:
    """
    header_words = VOCAB.header_words
    row_words = []
    [row_words.extend(li.split()) for li in text_list]
    row_words = set(row_words)
    mutual_words = row_words.intersection(header_words)
    return False if len(mutual_words) < len(row_words) / 2 else True


def is_binary_test(text_list):
    row = " ".join(text_list)
    if "negative" in row or "positive" in row or "am tinh" in unidecode(row) or "duong tinh" in unidecode(row):
        return True
    else:
        return False


def preprocess_text(texts_list_list):
    """
    drop redundant rows from text
    :param vocabulary:
    :param texts_list_list:
    :return:
    """
    ret = [[text.strip() for text in li] for li in texts_list_list]
    ret = [[text for text in li if len(text) > 0] for li in ret]
    ret = [li for li in ret if len(li) > 0]

    return ret


def detect_name_value_range(text_matrix, n_number_matrix, number_str_matrix, binary_test, header_row=None, master_data_dict=dict()):
    """

    :param text_matrix:
    :param number_str_matrix:
    :param n_number_matrix:
    :param binary_test:
    :return:
    """
    bin_test = [row for row in text_matrix if is_binary_test(row)]
    if len(bin_test) > 0:
        lab_names = get_all_lab_names(master_data_dict)
    print(bin_test)
    return 0, 1, 2

    n_test = len(n_number_matrix)
    n_col = len(n_number_matrix[0])
    n_number_columns = [[row[j] for row in n_number_matrix] for j in range(n_col)]
    columns = [[row[j] for row in text_matrix] for j in range(n_col)]
    column_words = [' '.join(col).split() for col in columns]
    name_id, value_id, range_id, unit_id = [], [], [], []
    for j in range(n_col):
        is_name, is_unit = False, False
        test_words = [w for w in column_words[j] if w in VOCAB.test_words]
        if len(test_words) > 0.3 * n_test:
            name_id.append(j)
            is_name = True
        unit_words = [w for w in column_words if w in VOCAB.unit_words]
        if len(unit_words) > 0.3 * n_test:
            unit_id.append(j)
            is_unit = True
        n_num_col_mode = get_mode(n_number_columns[j])
        if n_num_col_mode == 2:
            range_id = j
        elif n_num_col_mode == 1:
            value_id.append(j)
        elif n_num_col_mode == 0:
            if not is_name and is_unit:
                raise NotImplementedError
            else:
                raise NotImplementedError
        else:
            raise NotImplementedError

    return name_id, value_id, range_id, unit_id


def text2lab_details(text, master_data_dict, vocabulary):
    """
    Convert string-list-list-text input text to string-json output lab results
    :param texts:
    :param master_data_dict:
    :return:
    """
    if isinstance(text, list):
        texts_list_list = text
    elif isinstance(text, str):
        texts_list_list = json.loads(text)
    else:
        return 'INVALID INPUT TYPE'
    # preprocessing
    texts_list_list = preprocess_text(texts_list_list)

    row_len = [len(li) for li in texts_list_list]
    n_col = max(row_len)
    mode_len = get_mode(row_len)
    texts_list_list = [li for li in texts_list_list if len(li) >= mode_len]
    n_row = len(texts_list_list)

    # spelling correction
    ocr_backup = texts_list_list[:]
    texts_list_list = [[jaccard_correct_str(text.lower(), vocabulary) for text in li] for li in texts_list_list]

    header_row = None
    header_index = -1
    for i in range(n_row):
        if is_header(texts_list_list[i]):
            header_index = i
            header_row = texts_list_list[i]
            break
    texts_list_list = [] if header_index == n_row - 1 else texts_list_list[header_index + 1:]
    text_matrix = [row + [""] * (n_col - len(row)) for row in texts_list_list]
    n_row = len(text_matrix)
    if n_row < 1:
        return [dict()]

    # category/classification
    number_str_matrix = [[re.findall(NUMBER_PATTERN, text) for text in row] for row in ocr_backup[header_index + 1:]]
    n_number_matrix = [[len(n_str) for n_str in row] for row in number_str_matrix]
    binary_test = [is_binary_test(test) for test in text_matrix]
    name_id, result_id, range_id = detect_name_value_range(text_matrix, n_number_matrix, number_str_matrix, binary_test,
                                                           header_row, master_data_dict)

    # post_process
    results = post_process(text_matrix, number_str_matrix, name_id, result_id, range_id, master_data_dict)
    return results


def post_process(text_matrix, number_str_matrix, name_id, result_id, range_id, master_data_dict):
    ret = []
    lab_names = get_all_lab_names(master_data_dict)
    print(lab_names)
    for i in range(len(text_matrix)):
        if is_header(text_matrix[i]):
            continue
        print(text_matrix[i])
        value = number_str_matrix[i][result_id]
        if len(value) > 0:
            value = float(value[0])
        else:
            value = get_binary_value(text_matrix[i][result_id])
        name = unidecode(text_matrix[i][name_id]).lower()  # .replace('[', '(')
        print(name)
        # if '(' in name and name[0] != '(':
        #     name = name[:name.find('(')]

        score, name = get_lab_name_jaccard(name, lab_names)
        name_value = {"name": name, "value": value}
        ms_data = master_data_dict.get(lab_names[name])
        row_res = dict(ms_data, **name_value)
        row_res['score'] = score

        raw_rg = text_matrix[i][range_id]
        rg = number_str_matrix[i][range_id]
        ms_range = row_res.get("ms_range")
        if len(rg) == 1 or (len(rg) == 2 and '^' in rg[1]):
            row_res['range'] = ms_range[:]
            not_none = 0 if ms_range[0] is not None else 1
            row_res['range'][not_none] = float(rg[0])
        elif len(rg) >= 2:
            row_res['range'] = [float(rg[0]), float(rg[1].strip('-'))]
        elif len(rg) == 0:
            row_res['range'] = get_binary_value(raw_rg)
        else:
            row_res['range'] = raw_rg
        # unit
        if len(rg) == 0:  # binary test
            row_res['unit'] = row_res['ms_unit']
        elif "^" in rg[-1]:
            _10 = rg[-1]
            row_res['unit'] = _10 + raw_rg[raw_rg.find(_10)+len(_10):].strip("[( )]")
        else:
            row_res['unit'] = raw_rg[raw_rg.find(rg[-1])+len(rg[-1]):].strip("[( )]")

        ret.append(row_res)
    return ret


def get_binary_value(value):
    values = {
        "negative": "negative",
        'am tinh': 'âm tính',
        "duong tinh": 'dương tính',
        "positive": "positive",
        "+": '+', "-": '-'
    }
    distance = []
    for val in values.keys():
        distance.append((levenshtein(unidecode(value.lower()), val), val))
    return values.get(sorted(distance)[0][1])


def get_lab_name_jaccard(appr_name, lab_names):
    if appr_name in lab_names:
        return 1.0, lab_names.get(appr_name)
    else:
        similarities = [(1 - (textdistance.Jaccard(qval=2).distance(v, appr_name)), lab_names.get(v)) for v in lab_names]
        return sorted(similarities)[-1]


if __name__ == "__main__":
    from data.master_data import M_data_mng
    data = M_data_mng('../data/Yersin Master Lab Code data.csv').master_data_dict
    # lab_names = dict()
    # for key in data.keys():
    #     lab = data.get(key)
    #     syn_names = lab.get("synonyms")
    #     for name in syn_names:
    #         lab_names[name] = lab.get("name")
    # name = get_lab_name("""LDL Cholesterol 4.46 II (3.60 mmol/L.)""", lab_names)
    # print(name)
    text2lab_details("HBsAb (Kháng thể VGSV B)\t121.2\t( <= 6.4 ) mIU/l", data)
