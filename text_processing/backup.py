import re

from text_processing.edit_distance import levenshtein
from text_processing.text2details import get_all_lab_names, NUMBER_PATTERN


def text2lab_details_bk(text, master_data_dict):
    """

    :param text:
    :param master_data_dict:
    :return:
    """
    lab_names = get_all_lab_names(master_data_dict)
    all_num_strs = re.findall(NUMBER_PATTERN, text)

    tlow = text.lower()
    if (
        "range" in tlow
        or "matology" in tlow
        or "chemistry" in tlow
        or ("công" in tlow and "máu" in tlow)
    ):
        return None

    if len(all_num_strs) > 0:
        value = all_num_strs[0]
        name = text[0:text.find(value)].strip()
        score, name = get_lab_name(name, lab_names)
        ret = {"name": name, "value": float(value)}
        ms_data = master_data_dict.get(lab_names[name])
        ret = dict(ms_data, **ret)
        # range
        ms_range = ret.get("ms_range")
        if len(ms_range) == 1:
            ret['range'] = ms_range
        else:
            rg = [item for item in ms_range if item is not None]
            if len(rg) == 1:
                ret['range'] = ms_range[:]
                not_none = 0 if ms_range[0] is not None else 1
                ret['range'][not_none] = float(all_num_strs[1])
            else:
                ret['range'] = [float(all_num_strs[1]), float(all_num_strs[2].strip('-'))]
        # unit
        last_num = all_num_strs[-1]
        if '^' in last_num:
            ret['unit'] = text[text.find(last_num):].strip("[(\n )]")
        else:
            ret['unit'] = text[text.find(last_num)+len(last_num):].strip("[(\n )]")
    else:
        vr = [w for w in text.split() if 'tive' in w]
        value, range = vr[0], vr[-1]
        name = text[0:text.find(value)].strip()
        score, name = get_lab_name(name, lab_names)
        ret = {"name": name, "value": value.lower()}
        ms_data = master_data_dict.get(lab_names[name])
        ret = dict(ms_data, **ret)
        ret['range'] = range.lower()
        ret['unit'] = text[text.rfind(range) + len(range):].strip("[(\n )]")
    ret['score'] = score
    return ret


def get_lab_name(appr_name, lab_names):
    distance = []
    for name in lab_names:
        distance.append((levenshtein(appr_name.lower(), name), name))
    dist, name = sorted(distance)[0]
    return 1.0/(dist + 1), name