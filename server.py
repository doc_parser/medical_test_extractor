import json
import time

from flask import Flask
from flask import request, jsonify

from text_processing.text2details import text2lab_details
from data.master_data import M_data_mng
from data.vocabulary import Vocabulary

MASTER_DATA = M_data_mng().master_data_dict
VOCAB = Vocabulary().vocab

app = Flask(__name__, static_url_path='')


class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv["message"] = self.message
        return rv


@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route("/hello", methods=['GET'])
def hello():
    return "Hello, world!"


@app.route("/get_lab_details", methods=['GET', 'POST'])
def get_lab_details():
    start = time.time()
    try:
        texts = json.loads(request.data)
    except Exception as e:
        return f"Error: {e}"
    ret = jsonify(text2lab_details(texts, MASTER_DATA, VOCAB))
    print(f"Processing time: {time.time() - start}")
    return ret


if __name__ == '__main__':
    print(MASTER_DATA)
    app.run(host='0.0.0.0', port=5000, debug=True)
