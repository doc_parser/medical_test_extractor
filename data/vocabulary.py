# -*- coding: utf-8 -*-
import os


def read_words(path):
    if os.path.isfile(path):
        return set(open(path, 'rt').read().strip().split('\n'))
    else:
        return set()


class Vocabulary():
    def __init__(self, path='./data'):
        header_file = os.path.join(path, 'header_words.txt')
        test_file = os.path.join(path, 'test_words.txt')
        unit_file = os.path.join(path, 'unit_words.txt')
        other_file = os.path.join(path, 'other_words.txt')
        self.header_words = read_words(header_file)
        self.test_words = read_words(test_file)
        self.unit_words = read_words(unit_file)
        self.other_words = read_words(other_file)
        self.vocab = self.header_words.union(self.test_words).union(self.unit_words).union(self.other_words)


if __name__ == "__main__":
    print(Vocabulary('.').vocab)