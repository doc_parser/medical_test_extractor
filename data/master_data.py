import csv
import json


def range_text2list(rtext):
    """
    convert '[1.6,6.1]' to [1.6, 6.1]
    :param rtext:
    :return:
    """
    ret = [None, None]
    if '<' in rtext:
        ret[1] = rtext.strip("[< =]")
        ret[1] = float(ret[1][:-1] if ret[1][-1] in ['+', '-'] else ret[1])
    elif '>' in rtext:
        ret[0] = rtext.strip("[> =]")
        ret[0] = float(ret[0][:-1] if ret[0][-1] in ['+', '-'] else ret[0])
    elif 'tive' in rtext or 'tính' in rtext:
        ret = [rtext.strip("[]").lower()]
    else:
        ret = json.loads(rtext)
    return ret


class M_data_mng():
    def __init__(self, path='./data/Yersin Master Lab Code data.csv'):
        """
        Load master data from TSV file p
        :param path: ath
        """
        self.file_path = path
        self.master_data_dict = self.load_matster_data(path)

    def load_matster_data(self, path):
        master_data = dict()
        with open(path) as csv_file:
            csv_reader = csv.DictReader(csv_file, delimiter='\t', quotechar='"')
            for lab in csv_reader:
                lab_data = dict()
                lab_data["name"] = lab.get("Name").strip().lower()
                lab_data["code"] = lab.get("loinc-code").strip()
                lab_data["ms_unit"] = lab.get("Unit").strip()
                lab_data["ms_range"] = range_text2list(lab.get("Range"))
                lab_data["synonyms"] = []
                for key in lab.keys():
                    if "syn-" in key:
                        syn = lab.get(key).strip()
                        if syn != "":
                            lab_data["synonyms"].append(syn.lower())
                master_data[lab_data["name"]] = lab_data
        return master_data

    def update_data(self):
        self.master_data_dict = self.load_matster_data(self.file_path)